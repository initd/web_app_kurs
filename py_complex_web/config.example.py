# coding: utf-8
import os

# SERVER #
APP_NAME = "guest_book_server"
DEBUG_MODE = False
SERVER_LOG_NAME = "/var/log/guestbook.com/server-errors.log"

LISTEN_PORT = 10001
LISTEN_HOST = "0.0.0.0"
HOST = "guestbook.com"

# DB #
DB_TYPE = "postgresql"  # postgresql | mysql | sqlite
DB_HOST = "localhost"
DB_PORT = 5432
DB_USER = "test_user"
DB_PASS = "test_password"
DB_NAME = "test_db"      # path to database file if SQLite
DB_DEBUG = False

# REDIS #
REDIS_DB = 0
REDIS_HOST = "127.0.0.1"
REDIS_PORT = 6379

# SESSIONS #
SESSION_AUTO_SAVE = True
SESSION_AUTO_START = True
SESSION_KEY = "PHPSESSID"
SESSION_SALT = "tetststettedfsdferre"
SESSION_TIME = 86400
SESSION_STORAGE = "redis"
SESSION_PREFIX = "test_srvc_sess_"
SESSION_REVERS_PREFIX = "test_srvc_reverse_"
SESSION_UNIQUE = True

# SECURE #
TOKEN_SALT = "tettetstetsteststetstestettset"

# BUSINESS LOGIC #

BL_PAGESIZE = 1000
BL_ITEMS_PER_PAGE = 25
BL_ADMIN_USERS_ITEMS_PER_PAGE = 50
BL_ADMIN_ID = 1

# QUEUES #
HOT_QUEUE_NAME = "test_queue"
HOT_MESSAGES_QUEUE_NAME = "test_msg_queue"
HOT_MODERATION_QUEUE_NAME = "test_moder_queue"

# EMAIL #
EMAIL_USER_NAME = 'user@example.com'
EMAIL_USER_PSW = 'some_password'
EMAIL_SMTP_SERVER = 'smtp.gmail.com'
EMAIL_SMTP_PORT = 587
EMAIL_CODEPAGE = 'utf-8'
EMAIL_NAME_FROM = u'Guest book service'

# ASYNC QUEUE WORKERS #
USE_ASYNC_WORKERS = True

# STATIC FILES #
# path on FS, where are static files, and on which url they are
STATIC_PATH = "/var/www/sceleton.com/py_web_static"
STATIC_URL = "/py_web_static"

try:
    from py_complex_web.config_dev import *
except ImportError:
    pass

try:
    from py_complex_web.config_strings import *
except ImportError:
    pass


DIR_ROOT = os.path.dirname(os.path.realpath(__file__))
DIR_UPLOADS = "/tmp"
