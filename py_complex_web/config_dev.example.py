# coding: utf-8

DEBUG_MODE = True

DB_TYPE = "sqlite"
DB_NAME = "db_name"      # path to database file if SQLite
DB_DEBUG = True         # output sql queries

STATIC_PATH = "./../py_web_static"

SESSION_STORAGE = "files"

USE_ASYNC_WORKERS = False
