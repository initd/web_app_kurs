# coding: utf-8
import argparse

fixture_utf_8_file = 'obscenities_words_utf8.txt'
fixture_unicode_file = 'obscenities_words_unicode.txt'
fixture_utf_8_default_out_file = 'obscenities_words_utf8_new.txt'
fixture_unicode_default_out_file = 'obscenities_words_unicode_new.txt'


def fixture_from_raw_unicode_test(input_file=fixture_unicode_file, *args):
    with open(input_file) as fixture_file:
        for line in fixture_file:
            line = line.decode('utf-8').decode('raw_unicode_escape')
            line = line.strip().encode('utf-8')
            print line


def fixture_from_utf8_test(input_file=fixture_utf_8_file, *args):
    with open(input_file) as fixture_file:
        for line in fixture_file:
            line = line.decode('utf-8').encode('raw_unicode_escape')
            line = line.strip().encode('utf-8')
            print line


def fixture_recode_from_raw_unicode_to_utf8(
        input_file=fixture_unicode_file,
        output_file=fixture_utf_8_default_out_file):
    with open(input_file) as fixture_file:
        with open(output_file, 'w') as fixture_file_out:
            for line in fixture_file:
                line = line.decode('utf-8').decode('raw_unicode_escape')
                fixture_file_out.write(line.encode('utf-8'))


def fixture_recode_from_utf8_to_raw_unicode(
        input_file=fixture_utf_8_file,
        output_file=fixture_unicode_default_out_file):
    with open(input_file) as fixture_file:
        with open(output_file, 'w') as fixture_file_out:
            for line in fixture_file:
                line = line.decode('utf-8').encode('raw_unicode_escape')
                fixture_file_out.write(line.encode('utf-8'))


test_mapping = {
    'u28': fixture_from_raw_unicode_test,
    '82u': fixture_from_utf8_test,
}

convert_mapping = {
    'u28': fixture_recode_from_raw_unicode_to_utf8,
    '82u': fixture_recode_from_utf8_to_raw_unicode,
}


def parse_args():
    description = ('Convert obscenities words from raw unicode to utf-8 '
                   'and vise versa.')
    parser = argparse.ArgumentParser(
        description=description)
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '-82u', '--utf8_to_unicode',
        action='store_const',
        const='82u',
        dest='mode',
        help='convert from utf8'
    )
    group.add_argument(
        '-u28', '--unicode_to_utf8',
        action='store_const',
        const='u28',
        dest='mode',
        help='convert from unicode'
    )
    parser.add_argument('-i', '--input', help='input file')
    parser.add_argument('-o', '--output', help='output file')
    test_help_msg = ('Run without any affects, '
                     'do not save output to file, only prints in stdout')
    parser.add_argument(
        '-t', '--test',
        action="store_true",
        help=test_help_msg)
    args = parser.parse_args()
    if not args.mode:
        parser.error("One of --process or --upload must be given")
    return args


def main():
    arguments = parse_args()
    print arguments
    mapping = convert_mapping
    if arguments.test:
        mapping = test_mapping
    run_fn = mapping[arguments.mode]
    print 'Staring function %s' % run_fn.__name__
    run_fn()


if __name__ == '__main__':
    main()
