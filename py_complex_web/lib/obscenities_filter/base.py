# coding: utf-8
from os.path import dirname, join
from py_complex_web.lib.morphy import api as morphy_api


class ObscenitiesFilter(object):
    _words_file_name = join(
        dirname(__file__),
        'convertor',
        'obscenities_words_unicode.txt'
    )

    def __init__(self):
        self.morphy_api = morphy_api
        self.morphy_api.load_words(self._load_words())

    def _load_words(self):
        with open(self._words_file_name) as words_file:
            words = set()
            for line in words_file:
                words.add(
                    line.decode('utf-8').decode('raw_unicode_escape').strip()
                )
            return words

    def check_obscenities(self, text):
        return bool(self.morphy_api.count_word_matches_in_text(text))
