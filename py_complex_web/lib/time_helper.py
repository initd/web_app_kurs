# coding: utf-8
""" Module with date/time shortcuts"""

import pytz
import time
import calendar
import datetime as dt

default_timezone = "UTC"
default_date_fmt = "%Y-%m-%d"
default_datetime_fmt = "%Y-%m-%d %H:%M:%S"


def dt_to_unixtime(datetime_):
    """ Create timestamp from datetime object"""
    return int(calendar.timegm(datetime_.timetuple()))


def dt_to_unixmtime(datetime_):
    """ Create timestamp from datetime object"""
    ms = datetime_.microsecond / 1000
    return dt_to_unixtime(datetime_) * 1000 + ms


def unixtime_curr():
    """ Get local timestamp (timezone from ENV)"""
    temp = dt.datetime.now()
    return dt_to_unixtime(temp)


def unixtime_curr_utc():
    """ Get local timestamp in UTC (10 digits, int)"""
    temp = dt.datetime.utcnow()
    return dt_to_unixtime(temp)


def unixmtime_curr_utc():
    """ Get local timestamp in UTC (13 digits, int)"""
    temp = dt.datetime.utcnow()
    return dt_to_unixmtime(temp)


def unixtime_curr_tz(timezone=None):
    """ Get local timestamp in some timezone.
        :param timezone: string - timezone like UTC, GMT, Europe/Moscow
        :return unixtime: int
    """
    if timezone is None:
        timezone = default_datetime_fmt

    tz_utc = pytz.timezone("UTC")
    tz_needle = pytz.timezone(timezone)

    temp = dt.datetime.now(tz=tz_utc)
    temp = temp.astimezone(tz_needle)

    return dt_to_unixtime(temp)


def unixtime_to_date(unixtime, datetime_fmt=None):
    """ Convert local unixtime to local datetime string
        :param unixtime: int - timestamp (local time)
        :param datetime_fmt: string - datetime format
        :return datetime string: string - date in datetime_fmt format
    """
    if datetime_fmt is None:
        datetime_fmt = default_date_fmt
    return dt.datetime.fromtimestamp(unixtime).strftime(datetime_fmt)


def unixtime_utc_to_dt(unixtime_utc):
    """ Convert UTC unixtime to UTC
        :param unixtime_utc: int - timestamp (UTC time)
        :return datetime
    """
    return dt.datetime.utcfromtimestamp(unixtime_utc)


def unixtime_utc_to_date_utc(unixtime_utc, date_fmt=None):
    """ Convert UTC unixtime to UTC
        :param unixtime_utc: int - timestamp (UTC time)
        :param date_fmt: string - date format
        :return datetime string: string - UTC date in datetime_fmt format
    """
    if date_fmt is None:
        date_fmt = default_date_fmt
    return unixtime_utc_to_dt(unixtime_utc).strftime(date_fmt)


def unixtime_utc_to_datetime_utc(unixtime_utc, datetime_fmt=None):
    """ Convert UTC unixtime to UTC
        :param unixtime_utc: int - timestamp (UTC time)
        :param datetime_fmt: string - date format
        :return datetime string: string - UTC date in datetime_fmt format
    """
    if datetime_fmt is None:
        datetime_fmt = default_datetime_fmt
    return unixtime_utc_to_dt(unixtime_utc).strftime(datetime_fmt)


def unixtime_to_date_tz(unixtime, datetime_fmt=None, timezone=None):
    """ Convert local unixtime to datetime with timezone.
        :param unixtime: int - timestamp (local time)
        :param datetime_fmt: string - datetime format
        :param timezone: string - timezone like UTC, GMT, Europe/Moscow
        :return datetime string: string - timezone date in datetime_fmt format
    """
    if datetime_fmt is None:
        datetime_fmt = default_datetime_fmt

    if timezone is None:
        timezone = default_timezone

    tz = dt.tzinfo().tzname(timezone)
    if not tz:
        raise Exception("Invalid timezone %s" % timezone)
    return dt.datetime.fromtimestamp(unixtime).strftime(datetime_fmt)


def unixtime_to_datetime_tz(unixtime, datetime_fmt=None, timezone=None):

    if datetime_fmt is None:
        datetime_fmt = default_datetime_fmt

    if timezone is None:
        timezone = default_timezone

    tz = pytz.timezone(timezone)
    if not tz:
        raise Exception("Invalid timezone %s" % timezone)
    saved_dt = dt.datetime.fromtimestamp(unixtime)
    return tz.localize(saved_dt).strftime(datetime_fmt)


def timezone_curr_name():
    """ Get current timezone name
        :return name: string - name like UTC, GMT, Europe/Moscow
    """
    return time.strftime("%Z")


def timezone_curr_offset():
    """ Get current timezone offset (from UTC)
        :return offset: string - offset like +2000 or -08000
    """
    return time.strftime("%z")


def today_utc_timestamps():
    dt_ = dt.datetime.today()
    dt_ = dt.date(dt_.year, dt_.month, dt_.day)
    t = dt_to_unixtime(dt_)
    return t, t + 86400


def date_to_dt(string, date_fmt=None):
    if not date_fmt:
        date_fmt = default_date_fmt
    time_struct = time.strptime(string, date_fmt)
    return dt.datetime.fromtimestamp(time.mktime(time_struct))


if __name__ == "__main__":
    template = "%35s %10s %15s %11s"

    print template % (
        "timezone_curr_name", "", "", timezone_curr_name())

    print template % (
        "timezone_curr_offset", "", "", timezone_curr_offset())

    print template % (
        "unixtime_curr", "", "", unixtime_curr())

    print template % (
        "unixtime_curr_utc", "", "", unixtime_curr_utc())

    print template % (
        "unixtime_curr_tz", "", "UTC", unixtime_curr_tz(timezone="UTC"))

    print template % (
        "unixtime_curr_tz", "", "GMT", unixtime_curr_tz(timezone="GMT"))

    print template % (
        "unixtime_curr_tz", "", "Europe/Moscow",
        unixtime_curr_tz(timezone="Europe/Moscow"))

    print template % (
        "unixtime_curr_tz", "", "Europe/Kiev",
        unixtime_curr_tz(timezone="Europe/Kiev"))
