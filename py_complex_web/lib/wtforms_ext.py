# coding: utf-8
from wtforms.fields import Field
from wtforms.widgets import TextInput

from py_complex_web.lib.time_helper import date_to_dt
from py_complex_web.lib.time_helper import dt_to_unixtime
from py_complex_web.lib.time_helper import unixtime_to_date


def filter_strip(string):
    """Function instead of next lambda:
    filter_strip = lambda x: x.strip() if x else None
    :param string: string to strip or None
    :return: stripped string
    """
    if string:
        return string.strip()


class TimestampField(Field):
    widget = TextInput()

    def __init__(self, label=None, validators=None, fmt='%Y-%m-%d %H:%M:%S',
                 **kwargs):
        super(TimestampField, self).__init__(label, validators, **kwargs)
        self.format = fmt
        self.data = None

    def pre_validate(self, value):
        dt = date_to_dt(value.data.get("valid_thru"), date_fmt=self.format)
        self.data = dt_to_unixtime(dt)

    def _value(self):

        if self.raw_data:
            return ' '.join(self.raw_data)
        if self.data:
            return unixtime_to_date(self.data, datetime_fmt=self.format)
        return ""
