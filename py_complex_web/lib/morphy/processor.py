# coding: utf-8
import re
import pymorphy2


class PyMorphyProcessor(object):
    word_pattern = ur'[А-яЁёA-z\-]+'

    def __init__(self, words=tuple()):
        self.morph = pymorphy2.MorphAnalyzer()
        self.words = set(words)

    def _gen(self, text):
        for word in re.findall(self.word_pattern, text):
            if len(word) > 2:
                for morpheme in self.morph.parse(word.lower()):
                    if morpheme.normal_form in self.words:
                        yield word
                        break
            elif word in self.words:
                yield word

    def load_words(self, words):
        self.words = set(words)

    def check_text(self, text):
        return tuple(w for w in self._gen(text))

    def count_matches(self, text):
        return len(self.check_text(text))


morphy_processor = PyMorphyProcessor()
