# coding: utf-8
from py_complex_web.lib.morphy.processor import morphy_processor


def load_words(words):
    return morphy_processor.load_words(words)


def count_word_matches_in_text(text):
    return morphy_processor.count_matches(text)


def word_matches_in_text(text):
    return morphy_processor.check_text(text)
