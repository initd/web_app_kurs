# coding: utf-8
import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class MailSender(object):
    def __init__(self, host, port, user_name, password, name=None,
                 code_page='utf-8'):
        self.host = host
        self.port = port
        self.user_name = user_name
        self.password = password
        self.name = name if name else self.user_name.split('@', 1)[0]
        self.code_page = code_page

    def send_mail(self, to_user, subject, text_part, html_part=None):
        mail_server = smtplib.SMTP(host=self.host, port=self.port)
        mail_server.ehlo()
        mail_server.starttls()
        mail_server.ehlo()
        mail_server.login(self.user_name, self.password)
        msg = self.forming_msg(to_user, subject, text_part, html_part)
        mail_server.sendmail(self.user_name, to_user, msg.as_string())
        mail_server.quit()

    def forming_msg(self, to_user, subject, text_part, html_part=None):
        msg = MIMEMultipart('alternative')
        msg['From'] = self.name + ' <' + self.user_name + '>'
        msg['To'] = to_user
        msg['Subject'] = subject
        msg_text_part = MIMEText(text_part, 'plain', self.code_page)
        msg.attach(msg_text_part)
        if html_part:
            msg_html_part = MIMEText(html_part, 'html', self.code_page)
            msg.attach(msg_html_part)
        return msg


def main():
    """
    m_sender = MailSender(SMTP_SERVER, SMTP_PORT, USER_NAME, USER_PSW)
    m_sender.send_mail(USER_NAME, TEST_SUBJ, TEST_TEXT, TEST_HTML)"""
    pass

if __name__ == '__main__':
    main()
