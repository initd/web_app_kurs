# coding: utf-8

from bottle import request

from py_complex_web import config
from py_complex_web.lib.time_helper import unixtime_utc_to_date_utc
from py_complex_web.lib.time_helper import unixtime_utc_to_datetime_utc


def response_(data=None):
    result = {
        "REQUEST_PATH": request.path,
        "REQUEST_QUERY": request.query_string,
        "curr_date": unixtime_utc_to_date_utc(request.time10),
        "curr_datetime": unixtime_utc_to_datetime_utc(request.time10),
        "curr_unixtime": request.time10,
        "STATIC": config.STATIC_URL,
    }

    try:
        result["session"] = request.session
    except AttributeError:
        pass

    if data:
        result.update(data)
    return result


def response_ok(data=None):
    if not data:
        data = dict()
    data["status"] = "ok"
    return response_(data)


def response_error(data=None, errors=None):
    if not data:
        data = dict()

    data["status"] = "error"
    data["errors"] = errors or list()
    print data
    return response_(data)
