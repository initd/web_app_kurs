# coding: utf-8

import bcrypt
from time import time

T = int(time())


def gen_psw_hash(password):
    return bcrypt.hashpw(password, bcrypt.gensalt())

users = [
    dict(email="admin@ad.min",
         password=gen_psw_hash("admin"),
         time_created=T,
         time_activated=T,),
]
