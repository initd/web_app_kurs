# coding: utf-8

from pony.orm import db_session

from py_complex_web.dev.db.mocks.users import users
from py_complex_web.db.models import User


def fill_users():
    with db_session:
        for user in users:
            new_user = User(**user)
            new_user.save()


def all_data():
    fill_users()


if __name__ == '__main__':
    all_data()

