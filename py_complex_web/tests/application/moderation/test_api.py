# coding: utf-8
from time import time
import unittest

from py_complex_web import tests  # NEED TO PATCH DB CONNECTION
from py_complex_web.application.moderation import api as mod_api
from py_complex_web.db import db_session
from py_complex_web.db.models import GuestBookItem, BannedHost


class ModerationApiTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        with db_session:
            gb = GuestBookItem(
                store='Bla-bla, bla blabal blalbal',
                name='Name mumu',
                user=None,
                time_created=int(time())
            )
            gb.save()
            gb_obsc = GuestBookItem(
                store='\\u0425\\u0443\\u0439'.decode('raw_unicode_escape'),
                name='Name mumu',
                user=None,
                time_created=int(time())
            )
            gb_obsc.save()

            bh = BannedHost(host='127.0.0.1', time_banned=int(time()))
            bh.save()
        cls.bh_id = bh.id
        cls.gb_id = gb.id
        cls.gb_obsc_id = gb_obsc.id

    @classmethod
    def tearDownClass(cls):
        with db_session:
            GuestBookItem[cls.gb_id].delete()
            GuestBookItem[cls.gb_obsc_id].delete()
            BannedHost[cls.bh_id].delete()

    @db_session
    def test_moderate(self):
        self.assertEqual(mod_api.moderate(self.gb_id, host='127.0.0.2'), True)
        self.assertEqual(GuestBookItem[self.gb_id].time_activated > 0, True)

    @db_session
    def test_moderate_invalid_store_id(self):
        self.assertEqual(
            mod_api.moderate(self.gb_id + 1, host='127.0.0.2'), False
        )

    @db_session
    def test_moderate_banned_host(self):
        self.assertEqual(
            mod_api.moderate(self.gb_id, host='127.0.0.1'), False
        )
        self.assertEqual(GuestBookItem[self.gb_id].reason, 2)

    @db_session
    def test_moderate_obscenities(self):
        self.assertEqual(
            mod_api.moderate(self.gb_obsc_id, host='127.0.0.2'), False
        )
        self.assertEqual(GuestBookItem[self.gb_obsc_id].reason, 1)
