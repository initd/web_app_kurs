# coding: utf-8
from time import time
import unittest

from py_complex_web import tests  # NEED TO PATCH DB CONNECTION
from py_complex_web.application.moderation.host_filter import (
    StoreHostValidator)
from py_complex_web.db import db_session
from py_complex_web.db.models import BannedHost
from py_complex_web.tests.base import StoreMock


class HostValidatorTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        with db_session:
            bh = BannedHost(host='127.0.0.1', time_banned=int(time()))
            bh.save()
        cls.bh_id = bh.id
        cls.store_validator = StoreHostValidator()
        cls.store = StoreMock(u'Name', u'Some long, long text.')

    @classmethod
    def tearDownClass(cls):
        with db_session:
            BannedHost[cls.bh_id].delete()

    @db_session
    def test_host_filter(self):
        self.assertEqual(
            self.store_validator(self.store, host='127.0.0.1'), False
        )

    @db_session
    def test_host_filter_neg(self):
        self.assertEqual(
            self.store_validator(self.store, host='8.8.8.8'), True
        )
