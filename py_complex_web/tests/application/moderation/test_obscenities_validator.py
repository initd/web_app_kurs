# coding: utf-8
import unittest

from py_complex_web.application.moderation.obscenities_filter import (
    StoreObscenitiesValidator)
from py_complex_web.tests.base import StoreMock


class ObscenitiesValidatorTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.validator = StoreObscenitiesValidator()
        cls.name_with_obscenities = (
            '\\u0425\\u0443\\u0439'.decode('raw_unicode_escape')
        )
        cls.name_without_obscenities = u'Александр'
        cls.text_with_obscenities = (
            '\u0401\u0431\u0430\u043d\u043d\u044b\u0435 '
            '\u0432\u044b\u0431\u043b\u044f\u0434\u043a\u0438 \u0438 '
            '\u043c\u0440\u0430\u0437\u0438, '
            '\u0447\u0442\u043e\u0436 '
            '\u0432\u0430\u043c \u043d\u0435 '
            '\u0441\u043f\u0438\u0442\u0441\u044f \u0438 '
            '\u043a\u0430\u043a\u043e\u0433\u043e '
            '\u0445\u0443\u044f \u0432\u044b \u0442\u0443\u0442 '
            '\u0434\u0435\u043b\u0430\u0435\u0442, '
            '\u0435\u0431\u043d\u0438 \u0441\u0441\u0430\u043d\u044b\u0435, '
            '\u0451\u0431 \u0442\u0432\u043e\u044e \u043c\u0430\u0442\u044c, '
            '\u043f\u0438\u0434\u043e\u0440, '
            '\u043f \u0438 \u0434 \u043e \u0440, '
            '\u043f\u0438\u0434\u043e\u0440\u043e\u043a, '
            '\u043f\u0438\u0434\u043e\u0440\u043e\u043a4'
        ).decode('raw_unicode_escape')
        cls.text_without_obscenities = (
            u'Чёртовы соседи, чтож вам не спится и '
            u'какого чёрта вы тут делаете, редиски такие, цензура, гномик, '
            u'коркоед, крот, енот4'
        )
        cls.Store = StoreMock

    def prepare_store(self, is_obsc_name=False, is_obsc_text=False):
        self.store = self.Store(self.name_without_obscenities,
                                self.text_without_obscenities)
        if is_obsc_name:
            self.store.author = self.name_with_obscenities
        if is_obsc_text:
            self.store.store = self.text_with_obscenities

    def test_check_obscenities(self):
        combs = ((a, b) for a in (True, False) for b in (True, False))
        for comb in combs:
            self.prepare_store(*comb)
            if comb == (False, False):
                self.assertEqual(self.validator(self.store), True)
                continue
            self.assertEqual(self.validator(self.store), False)

    def test_check_without_obscenities(self):
        self.prepare_store(False, False)
        self.assertEqual(self.validator(self.store), True)
