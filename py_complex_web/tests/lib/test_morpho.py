# coding: utf-8
import unittest

from py_complex_web.lib.morphy import api as morphy_api, processor


class MorphyTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.text = (
            u'Отвратительно4 и бессовестно шумные соседи, гулящие допоздна, '
            u'что ж вам не спится в эту препрекраснейшую и тихую ночь, '
            u'полную звёзд в небе, чертовско обидно, н е б е, не-бе, неб е!'
        )
        cls.test_words = (u'отвратительный', u'бессовестный',
                          u'прекрасный', u'небо', u'чертовский')

        processor.morphy_processor.words = cls.test_words

    def test_count_matches(self):
        self.assertEqual(morphy_api.count_word_matches_in_text(self.text), 4)

    def test_founded_words(self):
        self.assertEqual(
            morphy_api.word_matches_in_text(self.text),
            (u'Отвратительно', u'бессовестно', u'небе', u'чертовско')
        )

    @unittest.skip(u'Word with spaces not implemented')
    def test_count_matches_with_spaces(self):
        self.assertEqual(morphy_api.count_word_matches_in_text(self.text), 7)

    def test_load_words(self):
        words = (u'не', u'шумный', u'звезда', u'есть', u'есть')
        morphy_api.load_words(words)
        self.assertEqual(processor.morphy_processor.words, set(words))
        self.assertEqual(morphy_api.count_word_matches_in_text(self.text), 3)
        self.assertEqual(
            morphy_api.word_matches_in_text(self.text),
            (u'шумные', u'не', u'звёзд')
        )
