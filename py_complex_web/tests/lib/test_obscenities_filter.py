# coding: utf-8
import unittest

from py_complex_web.lib.obscenities_filter.base import (
    ObscenitiesFilter)


class ObscenitiesFilterTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.text_with_obscenities = (
            '\u0401\u0431\u0430\u043d\u043d\u044b\u0435 '
            '\u0432\u044b\u0431\u043b\u044f\u0434\u043a\u0438 \u0438 '
            '\u043c\u0440\u0430\u0437\u0438, '
            '\u0447\u0442\u043e\u0436 '
            '\u0432\u0430\u043c \u043d\u0435 '
            '\u0441\u043f\u0438\u0442\u0441\u044f \u0438 '
            '\u043a\u0430\u043a\u043e\u0433\u043e '
            '\u0445\u0443\u044f \u0432\u044b \u0442\u0443\u0442 '
            '\u0434\u0435\u043b\u0430\u0435\u0442, '
            '\u0435\u0431\u043d\u0438 \u0441\u0441\u0430\u043d\u044b\u0435, '
            '\u0451\u0431 \u0442\u0432\u043e\u044e \u043c\u0430\u0442\u044c, '
            '\u043f\u0438\u0434\u043e\u0440, '
            '\u043f \u0438 \u0434 \u043e \u0440, '
            '\u043f\u0438\u0434\u043e\u0440\u043e\u043a, '
            '\u043f\u0438\u0434\u043e\u0440\u043e\u043a4'
        ).decode('raw_unicode_escape')
        cls.text_without_obscenities = (
            u'Чёртовы соседи, чтож вам не спится и '
            u'какого чёрта вы тут делаете, редиски такие, цензура, гномик, '
            u'коркоед, крот, енот4'
        )
        cls.method = ObscenitiesFilter().check_obscenities

    def test_check_obscenities(self):
        self.assertEqual(self.method(self.text_with_obscenities), True)

    def test_check_without_obscenities(self):
        self.assertEqual(self.method(self.text_without_obscenities), False)
