# coding: utf-8
import os.path
from py_complex_web import config
from py_complex_web.db import conn

temp_sqlite_file_name = os.path.join(os.path.dirname(__file__),
                                     'temp_test_db.sqlite')
config.DB_TYPE = 'sqlite'
config.DB_NAME = temp_sqlite_file_name

conn.db.disconnect()
reload(conn)
