# coding: utf-8
import sys
from unittest import TextTestResult, TextTestRunner, TestLoader  # , TestResult


def new_behaviour():
    suite = TestLoader().discover('.')
    TextTestRunner(verbosity=2).run(suite)


def old_behaviour():
    verbosity = 1
    # result = TestResult()
    result = TextTestResult(
        stream=sys.stderr, descriptions=True, verbosity=verbosity
    )
    test_suite = TestLoader().discover('.')
    test_suite.run(result=result)

if __name__ == '__main__':
    old_behaviour()
    new_behaviour()
