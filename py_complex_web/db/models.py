# coding: utf-8
from pony.orm import Set
from pony.orm import Optional
from pony.orm import Required
from pony.orm import PrimaryKey

from py_complex_web.db.conn import db
from py_complex_web.db.mixins import SiteItem
from py_complex_web.lib.time_helper import (
    unixtime_to_date, unixtime_to_datetime_tz)


default_date_fmt = "%d-%m-%Y"
default_datetime_fmt = "%d-%m-%Y %H:%M:%S"


class User(SiteItem, db.Entity):
    _table_ = "users"
    id = PrimaryKey(int, auto=True)
    email = Required(unicode, 255, unique=True)
    password = Required(unicode, 64)
    time_created = Required(int)
    time_activated = Optional(int, default=0)
    time_locked = Optional(int, default=0)
    time_deleted = Optional(int, default=0)
    guest_stores = Set("GuestBookItem", reverse="user")

    @property
    def datecreated(self):
        return unixtime_to_date(self.time_created, default_date_fmt)


class GuestBookItem(SiteItem, db.Entity):
    _table_ = "guest_book_stores"
    id = PrimaryKey(int, auto=True)
    store = Required(unicode, 1023)
    name = Optional(unicode, 253, nullable=True)
    user = Optional(User, nullable=True, reverse="guest_stores")
    reason = Optional(int, nullable=True)
    time_created = Required(int)
    time_activated = Optional(int, default=0)
    time_locked = Optional(int, default=0)
    time_deleted = Optional(int, default=0)

    status_enum = {
        "ok": u"Активна",
        "deleted": u"Удалена",
        "blocked": u"Заблокирована",
        "inactive": u"Неактивна"
    }

    status_reverse_enum = {v: k for k, v in status_enum.iteritems()}

    actions_enum = {
        "ok": ("block", "delete"),
        "deleted": ("recover",),
        "blocked": ("unblock", "delete"),
        "inactive": ("activate", "delete")
    }

    @property
    def timecreated(self):
        return unixtime_to_datetime_tz(self.time_created, default_datetime_fmt)

    @property
    def timeactivated(self):
        return (
            "-" if not self.time_activated else
            unixtime_to_datetime_tz(self.time_activated, default_datetime_fmt)
        )

    @property
    def author(self):
        return self.name or self.user.email.split("@", 1)[0]

    @property
    def status(self):
        status = self.status_enum["ok"]
        if self.time_deleted:
            status = self.status_enum["deleted"]
        elif self.time_locked:
            status = self.status_enum["blocked"]
        elif not self.time_activated:
            status = self.status_enum["inactive"]
        return status

    @property
    def actions(self):
        return self.actions_enum[self.status_reverse_enum[self.status]]


class BannedHost(SiteItem, db.Entity):
    _table_ = "banned_hosts"
    id = PrimaryKey(int, auto=True)
    host = Required(unicode, 255, unique=True)
    time_banned = Required(int)

    @property
    def timebanned(self):
        return unixtime_to_datetime_tz(self.time_banned, default_datetime_fmt)

db.generate_mapping(create_tables=True)
