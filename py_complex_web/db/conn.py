# coding: utf-8

from pony.orm import sql_debug
from pony.orm import Database

from py_complex_web import config


db = None

if not db:
    if config.DB_TYPE not in ["sqlite", "postgresql"]:
        print "Unknown database type"
        exit(-1)

    if config.DB_TYPE == "sqlite":
        db = Database("sqlite", config.DB_NAME, create_db=True)

    elif config.DB_TYPE == "postgresql":
        db = Database("postgres", host=config.DB_HOST, port=config.DB_PORT,
                      user=config.DB_USER, password=config.DB_PASS,
                      database=config.DB_NAME)
    sql_debug(config.DB_DEBUG)
