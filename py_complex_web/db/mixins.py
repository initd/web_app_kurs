# coding: utf-8

from pony.orm import commit
from pony.orm import select

from py_complex_web.config import BL_PAGESIZE


class SiteItem(object):
    @classmethod
    def get_items_count(cls, expr=None):
        """ Function counts items in collection
            :param expr: lambda: function for selecting data
            :return int: items count
        """
        if expr is None:
            expr = (x for x in cls)
        return select(expr).count()

    @classmethod
    def get_items_sum(cls, expr=None):
        """ Function counts items in collection
            :param expr: lambda: function for selecting data
            :return int: items count
        """
        if expr is None:
            expr = (x for x in cls)
        return select(expr).sum()

    @classmethod
    def get_items(cls, expr=None, paginator=None, order_by=None):
        """ Function create query and read data from db.
            :param expr: lambda: function for selecting data
            :param paginator: lib.paginator.Paginator: Paginator instance
            :param order_by: Order by field
            :return list: list of db_entity()
        """
        if order_by is None:
            order_by = 1
        if expr is None:
            expr = (x for x in cls)

        if paginator is None:
            page = 1
            pagesize = BL_PAGESIZE if BL_PAGESIZE else 25
        else:
            page = paginator.curr_page
            pagesize = paginator.items_per_page
        return select(expr).order_by(order_by).page(page, pagesize=pagesize)

    @classmethod
    def get_limited_items(cls, expr=None, order_by=None, limit=None):
        """ Function create query and read data from db.
            :param expr: lambda: function for selecting data
            :param limit: for limiting query results count
            :param order_by: Order by field
            :return list: list of db_entity()
        """
        if order_by is None:
            order_by = 1
        if expr is None:
            expr = (x for x in cls)

        limit = limit if limit else BL_PAGESIZE

        return select(expr).order_by(order_by).limit(limit)

    @classmethod
    def get_dict_items(cls, *args, **kwargs):
        """ Function create query and read data from db.
            All objects will presented as dict objects (instead of db_entity())
            :return list: list of db_entity() as dict
        """
        items = []
        for x in cls.get_items(*args, **kwargs):
            item = {}
            for k in x.__class__.__dict__:
                v = getattr(x, k)
                if k.startswith("_") or k.endswith("_"):
                    continue
                if callable(v):
                    continue
                item[k] = v
            items.append(item)
        return items

    @staticmethod
    def save():
        commit()


class TreeMixin(object):
    @property
    def tree_level(self):
        return self.tree_path.count("/") - 1


class TransactionMixin(object):
    TRN_TYPES = {
        u"SystemPay": 0,
        u"SystemDmnLimitChange": 1,
        u"AdminPay": 2,
        u"AdminDmnLimitChange": 3,
        u"SelfDmnLimitExtend": 4,
        u"PromoPay": 5,
        u"ExternalPay": 6,
    }
