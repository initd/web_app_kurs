# coding: utf-8
from bottle import jinja2_view as view
from bottle import route

from py_complex_web.db.models import User, GuestBookItem
from py_complex_web.lib.response import response_ok
from py_complex_web.lib.auth import require_auth, require_admin


@route("/admin")
@require_auth
@require_admin
@view("views/admin/index.html")
def route_admin_dashboard():
    total_users = User.get_items_count()
    total_stores = GuestBookItem.get_items_count()
    expr = (store for store in GuestBookItem if store.time_activated)
    total_stores_activated = GuestBookItem.get_items_count(expr)
    expr = (store for store in GuestBookItem
            if store.reason is None and not store.time_activated)
    total_stores_awaiting = GuestBookItem.get_items_count(expr)
    data = dict(total_users=total_users,
                total_stores=total_stores,
                total_stores_activated=total_stores_activated,
                total_stores_awaiting=total_stores_awaiting)
    return response_ok({"data": data})
