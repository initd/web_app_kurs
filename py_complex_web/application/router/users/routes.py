# coding: utf-8
from urllib import unquote

import bcrypt
from bottle import route
from bottle import request
from bottle import redirect
from bottle import jinja2_view as view
from py_complex_web.application.notificator.messages_senders import email
from py_complex_web.application.config import config_accessor
from py_complex_web.application.router.users.forms import (
    UserLoginForm, UserVerifyForm, NewPasswordForm,
    UserRegisterForm, AdminUserChangeForm, UserPasswordChangeForm,
    UserPasswordRequestForm
)
from py_complex_web.db import ObjectNotFound
from py_complex_web.db.models import User
from py_complex_web.lib.auth import TokenGenerator
from py_complex_web.lib.auth import (
    require_auth, require_session_email, require_admin)
from py_complex_web.lib.response import response_ok
from py_complex_web.lib.paginator import Paginator


tkn_gen = TokenGenerator()


def get_admin_users_items_per_page():
    return config_accessor.get_admin_users_items_per_page()


def get_global_admin_id():
    return config_accessor.get_global_admin_id()


def get_admin_ids():
    admins_ids = set()
    admins_ids.add(get_global_admin_id())
    return admins_ids


@route("/users/register")
@view("views/users/registration.html")
def route_user_register():
    return response_ok({"form": UserRegisterForm()})


@route("/users/register", method="POST")
@view("views/users/registration.html")
def route_user_register_post():
    form = UserRegisterForm(request.forms.decode())
    if not form.validate():
        return response_ok({"form": form})

    psw = form.data.get("password").encode("utf-8")
    psw_hash = bcrypt.hashpw(psw, bcrypt.gensalt())

    new_user = User(email=form.data.get("email").lower(),
                    password=psw_hash,
                    time_created=request.time10,
                    )
    new_user.save()

    data = dict(email=new_user.email,
                psw=psw,
                token=tkn_gen.generate_email_activation_token(new_user.email),
                link=tkn_gen.generate_email_activation_link(new_user.email))
    email.send_register_email(new_user.email, data)

    #  set session
    request.session.set_user_info_invalid_actuality()

    request.session["email"] = new_user.email
    request.session["id_user"] = new_user.id

    request.session.flash(u"На вашу почту было отправлено "
                          u"сообщение с ссылкой активации", "success")
    return redirect("/users/profile")


@route("/users/login")
@view("views/users/login.html")
def route_user_login():
    if not request.session.is_guest:
        return redirect("/guestbook")
    return response_ok({"form": UserLoginForm(),
                        "redirect_url": request.query.get(
                            "redirect_url", "/guestbook")})


@route("/users/login", method="POST")
@view("views/users/login.html")
def route_user_login_post():
    if not request.session.is_guest:
        return redirect("/guestbook")

    form = UserLoginForm(request.forms.decode())
    if not form.validate():
        return response_ok({"form": form})

    user = User.get(email=form.data.get("email"))
    if not user:
        form.email.errors.append(u"Неверная пара email/пароль")
        return response_ok({"form": form})

    psw = form.data.get("password").encode("utf-8")
    if not bcrypt.hashpw(psw, user.password) == user.password:
        form.email.errors.append(u"Неверная пара email/пароль")
        return response_ok({"form": form})

    if user.time_locked:
        form.email.errors.append(u"Ваш аккаунт был заблокирован")
        return response_ok({"form": form})

    if user.time_deleted:
        form.email.errors.append(u"Ваш аккаунт был удален")
        return response_ok({"form": form})

    request.session["id_user"] = user.id
    request.session["email"] = user.email
    if user.id in get_admin_ids():
        request.session["is_admin"] = True

    redirect_url = request.query.get("redirect_url", "/guestbook")
    return redirect(unquote(redirect_url))


@require_auth
@route("/users/logout")
@view("views/users/logout.html")
def route_user_logout():
    request.session.invalidate()
    return response_ok()


@route("/users/logout", method="POST")
def route_user_logout():
    request.session.invalidate()
    return redirect("/users/login")


@route("/users/password/request")
@view("views/users/remind.html")
def route_user_password_request():
    return response_ok({"form": UserPasswordRequestForm()})


@route("/users/password/request", method="POST")
@view("views/users/remind.html")
def route_user_password_request_post():
    form = UserPasswordRequestForm(request.forms.decode())
    if not form.validate():
        return response_ok({"form": form})

    curr_email = form.data.get("email").lower()
    request.session["email"] = curr_email
    curr_user = User.get(email=curr_email)
    data = dict(email=curr_email,
                token=tkn_gen.generate_password_change_token(
                    curr_user.password),
                link=tkn_gen.generate_password_change_link(curr_email,
                                                           curr_user.password))
    email.send_password_remind_email(curr_email, data)
    request.session.flash(u"На вашу почту было отправлено письмо с ссылкой "
                          u"для восстановления пароля", "info")
    return redirect("/users/password/set")


@route("/users/password/set")
@view("views/users/set_password.html")
def route_user_password_set():
    form = NewPasswordForm()
    if request.query.get("token"):
        form = NewPasswordForm(data={"token": request.query.get("token")})
    if request.query.get("email"):
        try:
            curr_email = request.query.get("email").lower()
            curr_user = User.get(email=curr_email)
            request.session["email"] = curr_user.email
        except ObjectNotFound:
            request.session.flash(u"Пользователь с таким email не существует")
            return redirect("/users/profile")
    return response_ok({"form": form})


@route("/users/password/set", method="POST")
@require_session_email
@view("views/users/set_password.html")
def route_user_password_set_post():
    form = NewPasswordForm(request.forms.decode())
    if not form.validate():
        return response_ok({"form": form})
    curr_user = User.get(email=request.session.data.get("email"))
    token = tkn_gen.generate_password_change_token(curr_user.password)
    if not form.data.get("token") == token:
        form.token.errors.append(u"Неправильный код")
        return response_ok({"form": form})
    psw = form.data.get("new_password").encode("utf-8")
    psw_hash = bcrypt.hashpw(psw, bcrypt.gensalt())
    curr_user.password = psw_hash
    curr_user.save()
    request.session.flash(u"Пароль был успешно изменен", "success")
    return redirect("/users/login")


@route("/users/activate")
@view("views/users/activation.html")
def route_user_activate():
    form = UserVerifyForm()
    if request.query.get("token"):
        form = UserVerifyForm(data={"token": request.query.get("token")})

    if request.query.get("email"):
        try:
            curr_email = request.query.get("email").lower()
            curr_user = User.get(email=curr_email)
            request.session["email"] = curr_user.email
        except ObjectNotFound:
            request.session.flash(u"Пользователь с таким email не существует")
            return redirect("/users/profile")

    return response_ok({"form": form})


@route("/users/activate", method="POST")
@require_session_email
@view("views/users/activation.html")
def route_user_activate_post():
    form = UserVerifyForm(request.forms.decode())
    if not form.validate():
        return response_ok({"form": form})

    curr_email = request.session.data.get("email")
    curr_user = User.get(email=curr_email)
    token = tkn_gen.generate_email_activation_token(curr_email)
    if form.data.get("token") == token:
        curr_user.time_activated = request.time10
        curr_user.save()
        request.session.flash(u"Email успешно активирован", "success")
        return redirect("/users/profile")

    form.token.errors.append(u"Неправильный код")
    return response_ok({"form": form})


@route("/users/activate/request_email", method="POST")
@require_session_email
@view("views/users/activation.html")
def route_request_duplicate_activation_email():
    curr_email = request.session.data.get("email")
    data = dict(email=curr_email,
                token=tkn_gen.generate_email_activation_token(curr_email),
                link=tkn_gen.generate_email_activation_link(curr_email))
    email.send_activation_email(curr_email, data)
    request.session.flash(u"На вашу почту было повторно отправлено письмо с "
                          u"инструкциями по активации.", "info")
    return redirect("/users/activate")


@route("/users/profile")
@require_auth
@view("views/users/profile.html")
def route_user_edit_profile():
    curr_user = User[request.session.data.get("id_user")]
    details_form = UserRegisterForm(obj=curr_user)
    password_form = UserPasswordChangeForm()
    return response_ok({"details_form": details_form,
                        "password_form": password_form,
                        "curr_user": curr_user})


@route("/users/save", method="POST")
@require_auth
@view("views/users/profile.html")
def route_user_save():
    curr_user = User[request.session.data.get("id_user")]
    stored_user_email = curr_user.email

    details_form = UserRegisterForm(request.forms.decode())
    del details_form.password
    if not details_form.validate():
        return response_ok({"details_form": details_form,
                            "password_form": UserPasswordChangeForm(),
                            "curr_user": curr_user})

    is_email_changed = False
    if details_form.data.get("email").lower() != stored_user_email:
        curr_user.email = details_form.data.get("email").lower()
        curr_user.time_activated = 0
        is_email_changed = True

    curr_user.save()

    if is_email_changed:
        data = dict(email=stored_user_email,
                    new_email=curr_user.email,
                    id=curr_user.id)
        email.send_login_change_email(stored_user_email, data)
        data = dict(email=curr_user.email,
                    token=tkn_gen.generate_email_activation_token(
                        curr_user.email),
                    link=tkn_gen.generate_email_activation_link(
                        curr_user.email))
        email.send_activation_email(curr_user.email, data)
        request.session["email"] = curr_user.email
        request.session.flash(u"Email успешно изменен", "success")

    request.session.flash(u"Настройки успешно сохранены", "success")
    return redirect("/users/profile")


def user_pass_change(user, psw):
    psw_hash = bcrypt.hashpw(psw, bcrypt.gensalt())
    user.password = psw_hash
    user.save()


@route("/users/password/change", method="POST")
@require_auth
@view("views/users/profile.html")
def route_password_change():
    curr_user = User[request.session.data.get("id_user")]
    details_form = UserRegisterForm(obj=curr_user)
    password_form = UserPasswordChangeForm(request.forms.decode())
    if not password_form.validate():
        return response_ok({"details_form": details_form,
                            "password_form": password_form,
                            "curr_user": curr_user})

    psw = password_form.data.get("old_password").encode("utf-8")
    if not bcrypt.hashpw(psw, curr_user.password) == curr_user.password:
        password_form.old_password.errors.append(u"Неправильный пароль")
        return response_ok({"details_form": details_form,
                            "password_form": password_form,
                            "curr_user": curr_user})

    psw = password_form.data.get("new_password").encode("utf-8")
    user_pass_change(curr_user, psw)
    data = dict(email=curr_user.email,
                psw=psw)
    email.send_password_change_email(curr_user.email, data)
    request.session.flash(u"Пароль успешно изменен", "success")
    return redirect("/users/profile")


@route("/admin/users")
@route("/admin/users-<page:int>")
@require_auth
@require_admin
@view("views/users/admin_index.html")
def route_admin_users_index(page=None):
    if request.query.get("ident"):
        message = u"Пользователя c email'ом %s не существует"
        curr_ident = request.query.get("ident")
        try:
            id_user = int(curr_ident)
            user = User[id_user]
        except ValueError:
            user = User.get(email=curr_ident)
        except ObjectNotFound:
            message = u"Пользователя с id %s не существует"
            user = None
        if user:
            return redirect("/admin/users/%d" % user.id)
        request.session.flash(message % curr_ident)
        return redirect("/admin/users")

    page = 1 if not page else page
    template = "/admin/users-{page}"
    items_per_page = get_admin_users_items_per_page()
    paginator = Paginator(curr_page=page,
                          template=template,
                          items_per_page=items_per_page,
                          total_items_count=User.get_items_count())
    users = User.get_items(paginator=paginator)
    enumerated_users = enumerate(users, start=items_per_page*(page - 1) + 1)
    return response_ok({"enumerated_users": enumerated_users,
                        "paginator": paginator})


@route("/admin/users/<id_user:int>")
@require_auth
@require_admin
@view("views/users/admin_user_info.html")
def route_admin_user_by_id(id_user):
    try:
        curr_user = User[id_user]
    except ObjectNotFound:
        return redirect("/admin/users")

    form = AdminUserChangeForm()

    return response_ok({"user": curr_user,
                        "form": form})


@route("/admin/users/<id_user:int>/change", method="POST")
@require_auth
@require_admin
@view("views/users/admin_user_info.html")
def route_admin_change_user_by_user_id(id_user):
    try:
        curr_user = User[id_user]
    except ObjectNotFound:
        return redirect("/admin/users")

    form = AdminUserChangeForm(request.forms.decode())

    if not form.validate():
        return response_ok({"user": curr_user,
                            "form": form})

    if form.data.get("password"):
        psw = form.data.get("password").encode("utf-8")
        user_pass_change(curr_user, psw)
        request.session.flash(u"Пароль пользователя изменен.", "success")

    curr_user.save()
    return redirect("/admin/users/%s" % curr_user.id)
