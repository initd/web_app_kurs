# coding: utf-8
from bottle import route
from bottle import request
from bottle import redirect
from bottle import jinja2_view as view

from py_complex_web.application.config import config_accessor
from py_complex_web.application.router.hosts.forms import BanHostForm
from py_complex_web.db.models import BannedHost
from py_complex_web.lib.auth import require_auth, require_admin
from py_complex_web.lib.paginator import Paginator
from py_complex_web.lib.response import response_ok


def get_host_or_redirect(banned_host_id):
    try:
        host = BannedHost[banned_host_id]
        return host
    except BannedHost:
        request.session.flash(u"Запись с id: %d не найдена" % banned_host_id,
                              "error")
        return redirect("/admin/hosts")


@route("/admin/hosts")
@route("/admin/hosts-<page:int>")
@require_admin
@require_auth
@view("views/hosts/admin_index.html")
def route_admin_banned_hosts_index(page=None, form=None):
    page = 1 if not page else page
    form = BanHostForm() if not form else form
    template = "/admin/hosts-{page}"
    items_per_page = config_accessor.get_items_per_page()
    paginator = Paginator(curr_page=page,
                          template=template,
                          items_per_page=items_per_page,
                          total_items_count=BannedHost.get_items_count())
    banned_hosts = BannedHost.get_items(paginator=paginator)
    enumerated_hosts = enumerate(banned_hosts, 1)
    return response_ok({"enumerated_hosts": enumerated_hosts,
                        "form": form,
                        "paginator": paginator})


@route("/admin/hosts", method="POST")
@route("/admin/hosts-<page:int>", method="POST")
@require_admin
@require_auth
@view("views/hosts/admin_index.html")
def route_ban_host(page=None):
    page = 1 if not page else page

    form = BanHostForm(request.forms.decode())
    if not form.validate():
        return route_admin_banned_hosts_index(page, form)

    host = form.data.get("host")
    new_banned_host = BannedHost(host=host,
                                 time_banned=request.time10)
    new_banned_host.save()

    request.session.flash(u"Хост %s внесён в таблицу забанненых" % host,
                          "success")

    return redirect("/admin/hosts-%d" % page)


@route("/admin/hosts/<banned_host_id:int>", method="DELETE")
@view("views/hosts/admin_index.html")
@require_admin
@require_auth
def route_unban_host(banned_host_id):
    banned_host = get_host_or_redirect(banned_host_id)
    banned_host.delete()
    banned_host.save()
    request.session.flash(u"Запись с id: %d успешно удалена" % banned_host_id,
                          "success")
    return redirect("/admin/hosts")


@route("/admin/hosts/<banned_host_id:int>/unban", method="POST")
@view("views/hosts/admin_index.html")
@require_admin
@require_auth
def route_unban_host(banned_host_id):
    banned_host = get_host_or_redirect(banned_host_id)
    banned_host.delete()
    banned_host.save()
    request.session.flash(u"Запись с id: %d успешно удалена" % banned_host_id,
                          "success")
    return redirect("/admin/hosts")
