# coding: utf-8
from wtforms import Form
from wtforms import StringField
from wtforms import validators

from py_complex_web.lib.wtforms_ext import filter_strip


class BanHostForm(Form):
    host = StringField(
        u"Хост или ip-адрес",
        [validators.Length(
            max=255,
            message=u"Длина хоста должна быть не более 255 символов"
            ),
         validators.DataRequired(message=u"Заполните поле"),
         ],
        filters=[filter_strip]

    )
