# coding: utf-8
from bottle import request

from wtforms import Form
from wtforms import StringField
from wtforms import TextAreaField
from wtforms import validators

from py_complex_web.lib.wtforms_ext import filter_strip


def validate_name(form, field):
    _ = form
    if request.session.data.get("email"):
        raise validators.StopValidation()


class CreateGuestBookStoreForm(Form):
    name = StringField(
        u"Имя",
        [validators.Length(
            max=253,
            message=u"Имя должно быть не более 253 символов"
            ),
         validate_name,
         validators.DataRequired(message=u"Заполните поле"),
         ],
        filters=[filter_strip]

    )
    store = TextAreaField(
        u"Запись",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(
             max=1023,
             message=u"Длина записи должна быть не более 1024 символов"
            )
         ],
        filters=[filter_strip]
    )


class AdminChangeGuestBookStoreForm(Form):
    name = StringField(
        u"Имя",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(
             max=253,
             message=u"Имя должно быть не более 253 символов"
            )
         ],
        filters=[filter_strip]

    )
    store = TextAreaField(
        u"Запись",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(
             max=1023,
             message=u"Длина записи должна быть менее 1024 символов"
            )
         ],
        filters=[filter_strip]
    )
