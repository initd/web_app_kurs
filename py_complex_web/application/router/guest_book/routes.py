# coding: utf-8
import time

from bottle import route, request, redirect
from bottle import jinja2_view as view

from py_complex_web.application.config import config_accessor
from py_complex_web.application.router.guest_book.forms import (
    CreateGuestBookStoreForm)
from py_complex_web.application.moderation import api as moderation_api

from py_complex_web.db import ObjectNotFound
from py_complex_web.db.models import GuestBookItem, User
from py_complex_web.lib.auth import require_auth, require_admin
from py_complex_web.lib.paginator import Paginator
from py_complex_web.lib.response import response_ok


@route("/guestbook")
@route("/guestbook-<page:int>")
@view("views/guestbook/index.html")
def route_guest_book_index(page=None, form=None):
    page = 1 if not page else page
    form = CreateGuestBookStoreForm() if not form else form
    template = "/guestbook-{page}"
    expr = (store for store in GuestBookItem if store.time_activated and
            not (store.time_locked or store.time_deleted))
    items_per_page = config_accessor.get_items_per_page()
    paginator = Paginator(curr_page=page,
                          template=template,
                          items_per_page=items_per_page,
                          total_items_count=GuestBookItem.get_items_count(
                              expr=expr))

    stores = GuestBookItem.get_items(expr=expr, paginator=paginator,
                                     order_by=GuestBookItem.id.desc())
    return response_ok({"stores": stores,
                        "form": form,
                        "paginator": paginator})


@route("/guestbook", method="POST")
@route("/guestbook-<page:int>", method="POST")
@view("views/guestbook/index.html")
def route_create_guest_book_store(page=None):
    page = 1 if not page else page

    form = CreateGuestBookStoreForm(request.forms.decode())
    if not form.validate():
        return route_guest_book_index(page, form)

    name = form.data.get("name")
    curr_user = (User[request.session.data.get("id_user")]
                 if request.session.data.get("id_user") else None)
    new_store = GuestBookItem(store=form.data.get("store"),
                              name=name,
                              user=curr_user,
                              time_created=request.time10)
    new_store.save()

    request.session.flash(u"Спасибо! Ваш отзыв будет записан в гостевую "
                          u"книгу после автомодерации", "success")
    remote_addr = request.headers.get("X-Real-IP", request.remote_addr)
    moderation_api.moderate(new_store.id, remote_addr)
    return redirect("/guestbook/store/%d" % new_store.id)


def get_store_or_redirect(store_id):
    try:
        store = GuestBookItem[store_id]
        return store
    except ObjectNotFound:
        request.session.flash(u"Запись с id: %d не найдена" % store_id,
                              "error")
        return redirect("/guestbook")


@route("/guestbook/store/<store_id:int>")
@view("views/guestbook/store.html")
def route_guest_book_store(store_id):
    store = get_store_or_redirect(store_id)
    if not (request.session.data.get("is_admin") or
            store.status == store.status_enum["ok"]):

        request.session.flash(u"Запись с id: %d ещё не прошла модерацию, "
                              u"либо удалена" % store_id,
                              "error")
        return redirect("/guestbook")
    return response_ok({"store": store})


@route("/admin/guestbook/store/<store_id:int>")
@view("views/guestbook/store.html")
@require_admin
@require_auth
def route_admin_guest_book_store(store_id):
    return route_guest_book_store(store_id)


@route("/admin/guestbook/store/<store_id:int>", method="PUT")
@view("views/guestbook/store.html")
@require_admin
@require_auth
def route_admin_guest_book_store(store_id):
    data = request.forms.decode()
    action = data.get("action")
    store = get_store_or_redirect(store_id)
    curr_time = int(time.time())
    if action == "block":
        store.time_locked = curr_time
    elif action == "activate":
        store.time_activated = curr_time
    elif action == "unblock":
        store.time_locked = 0
    elif action == "recover":
        store.time_deleted = 0
    store.save()
    request.session.flash(u"Запись с id: %d успешно изменена" % store_id,
                          "success")
    return redirect("/admin/guestbook/store/%d" % store_id)


@route("/admin/guestbook/store/<store_id:int>/change", method="POST")
@view("views/guestbook/store.html")
@require_admin
@require_auth
def route_admin_guest_book_store(store_id):
    data = request.forms.decode()
    action = data.get("action")
    store = get_store_or_redirect(store_id)
    curr_time = int(time.time())
    if action == "block":
        store.time_locked = curr_time
    elif action == "activate":
        store.time_activated = curr_time
    elif action == "unblock":
        store.time_locked = 0
    elif action == "recover":
        store.time_deleted = 0
    store.save()
    request.session.flash(u"Запись с id: %d успешно изменена" % store_id,
                          "success")
    return redirect("/admin/guestbook/store/%d" % store_id)


@route("/admin/guestbook/store/<store_id:int>", method="DELETE")
@view("views/guestbook/store.html")
@require_admin
@require_auth
def route_admin_guest_book_store(store_id):
    store = get_store_or_redirect(store_id)
    store.time_deleted = int(time.time())
    store.save()
    request.session.flash(u"Запись с id: %d успешно удалена" % store_id,
                          "success")
    return redirect("/admin/guestbook/store/%d" % store_id)


@route("/admin/guestbook/store/<store_id:int>/delete", method="POST")
@view("views/guestbook/store.html")
@require_admin
@require_auth
def route_admin_guest_book_store(store_id):
    store = get_store_or_redirect(store_id)
    store.time_deleted = int(time.time())
    store.save()
    request.session.flash(u"Запись с id: %d успешно удалена" % store_id,
                          "success")
    return redirect("/admin/guestbook/store/%d" % store_id)


@route("/admin/guestbook")
@route("/admin/guestbook-<page:int>")
@require_admin
@require_auth
@view("views/guestbook/admin_index.html")
def route_admin_guest_book_index(page=None):
    page = 1 if not page else page
    template = "/admin/guestbook-{page}"
    items_per_page = config_accessor.get_admin_users_items_per_page()
    paginator = Paginator(curr_page=page,
                          template=template,
                          items_per_page=items_per_page,
                          total_items_count=GuestBookItem.get_items_count())
    stores = GuestBookItem.get_items(paginator=paginator,
                                     order_by=GuestBookItem.id.desc())
    enumerated_stores = enumerate(
        stores, start=items_per_page * (page - 1) + 1)
    return response_ok({"enumerated_stores": enumerated_stores,
                        "paginator": paginator})
