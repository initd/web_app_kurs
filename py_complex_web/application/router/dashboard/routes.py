# coding: utf-8
import datetime as dt

from bottle import route
from bottle import jinja2_view as view

from py_complex_web.db.models import User, GuestBookItem
from py_complex_web.lib.response import response_ok
from py_complex_web.lib.time_helper import dt_to_unixtime


def days_to(days):
        return dt_to_unixtime(dt.datetime.now() + dt.timedelta(days=days))


@route("/")
@view("views/index.html")
def route_index():
    total_users = User.get_items_count()
    total_stores = GuestBookItem.get_items_count()
    expr = (store for store in GuestBookItem if store.time_activated)
    total_stores_activated = GuestBookItem.get_items_count(expr)
    data = dict(total_users=total_users,
                total_stores=total_stores,
                total_stores_activated=total_stores_activated)
    return response_ok({"data": data})
