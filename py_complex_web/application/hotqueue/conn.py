# coding: utf-8
from hotqueue import HotQueue

from py_complex_web import config


messages_queue = HotQueue(config.HOT_MESSAGES_QUEUE_NAME,
                          host=config.REDIS_HOST,
                          port=config.REDIS_PORT,
                          db=config.REDIS_DB)

moderation_queue = HotQueue(config.HOT_MODERATION_QUEUE_NAME,
                            host=config.REDIS_HOST,
                            port=config.REDIS_PORT,
                            db=config.REDIS_DB)
