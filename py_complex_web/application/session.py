# coding: utf-8
from random import randint

from py_complex_web.db import ObjectNotFound
from py_complex_web.db.models import User
from py_complex_web.lib.session import Session as SessionPrototype
from py_complex_web.lib.session import md5, time


# TODO: fix bug with flash kinds: if we have !more than one! flash, flash
# can! change self KIND random, not that on adding, see on a rendered help page
class Session(SessionPrototype):
    """
    Flashes kinds may be "success", "warning", "error", None - info
    """
    delta_time_to_update_user_info = 30  # in seconds

    @property
    def flash_count(self):
        return len(self.data.get("flash", list()))

    def flash(self, message, kind=None):
        if not self.data.get("flash"):
            self.data["flash"] = list()
        self.data["flash"].append(dict(text=message, kind=kind))

    def flash_yield(self, kind=None):
        for i, message in enumerate(self.data.get("flash")):
            if kind and (kind == message["kind"]):
                del self.data["flash"][i]
                yield message
            elif not kind:
                del self.data["flash"][i]
                yield message

    def update_session_user_info(self):
        try:
            User[self.data.get("id_user")]
        except ObjectNotFound:
            return
        self.data["time_updated"] = time()
        self.data["is_info_not_actual"] = False

    def get_user_info(self):
        delta_time = time() - self.data.get("time_updated", 0)
        if (delta_time > self.delta_time_to_update_user_info) \
                or self.data["is_info_not_actual"]:
            self.update_session_user_info()
        return dict(balance=self.data["balance"])

    def set_user_info_invalid_actuality(self):
        self.data["is_info_not_actual"] = True

    def generate_token(self, token_type, keyword="email"):
        _str = "%s:%s:%s" % (self.session_salt, keyword, time())
        token_type = "token_%s" % token_type
        self.data[token_type] = md5(_str).hexdigest()[:20]
        if token_type == "token_phone":
            self.data[token_type] = "%05d" % randint(9999, 99999)
