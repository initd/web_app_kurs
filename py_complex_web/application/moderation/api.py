# coding: utf-8
from time import time

from py_complex_web.application.hotqueue.conn import moderation_queue
from py_complex_web.application.moderation import (
    obscenities_filter, host_filter)
from py_complex_web import config
from py_complex_web.db import db_session, ObjectNotFound
from py_complex_web.db.models import GuestBookItem

moderation_chain = (host_filter.StoreHostValidator(),
                    obscenities_filter.StoreObscenitiesValidator())


def moderate_async(store_id, host):
    moderation_queue.put(dict(store_id=store_id, host=host))


@db_session
def moderate_sync(store_id, host):
    try:
        store = GuestBookItem[store_id]
    except ObjectNotFound:
        return False
    result = True
    for validator in moderation_chain:
        if not validator(store, host=host):
            store.reason = validator.identifier
            result = False
            break
    if result:
        store.reason = 0
        store.time_activated = int(time())
    store.save()
    return result

moderate = moderate_sync
if config.USE_ASYNC_WORKERS:
    moderate = moderate_async
