# coding: utf-8
from py_complex_web.application.moderation.base import StoreValidator
from py_complex_web.db.models import BannedHost


class StoreHostValidator(StoreValidator):
    _id = 2
    _reason = u"There is the store from banned host."

    def __init__(self):
        super(StoreHostValidator, self).__init__()

    @staticmethod
    def is_host_banned(host):
        if not host or BannedHost.get(host=host):
            return True
        return False

    def is_host_valid(self, host):
        return not self.is_host_banned(host)

    def __call__(self, store, *args, **kwargs):
        return self.is_host_valid(kwargs['host'])
