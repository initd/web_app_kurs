# coding: utf-8
from py_complex_web.application.moderation.base import StoreValidator
from py_complex_web.lib.obscenities_filter.base import ObscenitiesFilter


class StoreObscenitiesValidator(StoreValidator):
    _id = 1
    _reason = u"Obscenities lexis found in store entity"

    def __init__(self):
        super(StoreObscenitiesValidator, self).__init__()
        self.obscenities_checker = ObscenitiesFilter()

    def check_obscenities(self, text):
        return self.obscenities_checker.check_obscenities(text)

    def __call__(self, store, *args, **kwargs):
        result = not(self.check_obscenities(store.author) or
                     self.check_obscenities(store.store))
        return result
