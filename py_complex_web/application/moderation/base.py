# coding: utf-8
from abc import abstractmethod


class StoreValidator(object):
    _id = None
    _reason = ''

    def __init__(self):
        super(StoreValidator, self).__init__()

    @abstractmethod
    def __call__(self, store, *args, **kwargs):
        raise NotImplemented

    @property
    def identifier(self):
        return self._id

    @property
    def reason(self):
        return self._reason
