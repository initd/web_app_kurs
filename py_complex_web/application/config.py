# coding: utf-8
from py_complex_web import config


class ConfigAccessor(object):
    def __init__(self):
        self.__dict__.update(config.__dict__)
        self.prop_db_inst_correlation = dict(
            default_user_balance="BL_DEFAULT_USER_BALANCE"
        )

    def get_items_per_page(self):
        return self.BL_ITEMS_PER_PAGE

    def get_admin_users_items_per_page(self):
        return self.BL_ADMIN_USERS_ITEMS_PER_PAGE

    def get_upload_dir(self):
        return self.DIR_UPLOADS

    def get_global_admin_id(self):
        return self.BL_ADMIN_ID

    def get_host(self):
        return self.HOST


config_accessor = ConfigAccessor()
