# coding: utf-8
from py_complex_web.application.hotqueue.conn import moderation_queue
from py_complex_web.application.moderation.api import moderate_sync


def main():
    for message in moderation_queue.consume():
        moderate_sync(**message)


if __name__ == '__main__':
    main()
