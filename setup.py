#!/usr/bin/env python

import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


def get_version():
    version_xml = read('pom.xml')
    return version_xml[9:].split('</')[0]


def get_requirements_list(requirements):
    all_requirements = read(requirements)
    return all_requirements.splitlines()


def form_data_files_pathes(pathes_dict):
    data_files = []
    for prefix, path_from in pathes_dict.iteritems():
        for root, dirs, filenames in os.walk(path_from):
            files = []
            for filename in filenames:
                curr_filename = os.path.join(root, filename)
                files.append(curr_filename)
            if files:
                data_files.append((os.path.join(prefix, root), files))
    return data_files


def get_data_files():
    return form_data_files_pathes(
        {
            '/': 'etc',
            '/usr/share/py_complex_web': 'py_web_static',
        }
    )


setup(
    name='guest_book',
    version=1.0,
    description='Complex web framework',
    long_description=read('README.md'),
    url='http://rodion.link',
    author='Rodion Promyshlennikov <matrix or initd>',
    author_email='rodion.prm@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    data_files=get_data_files(),
    entry_points={
        'console_scripts': [
            'guest_book_db_init = py_complex_web.dev.db.fill:all_data'],
    },
    classifiers=[
        'Environment :: Linux',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],
    install_requires=get_requirements_list('requirements.txt'),
)
