# -*- coding: utf-8 -*-

import sys

sys.path.append("../../")

import bcrypt

from pony.orm import db_session

from mocks.users import users
from py_complex_web.db.models import User


psw_hash = bcrypt.hashpw("password", bcrypt.gensalt())
with db_session:
    for user in users:
        new_user = User(**user)
        new_user.save()
