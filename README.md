### Setup ###

* Fork, not clone.
* Edit config
* pip install -r requirements.txt

### Run ###

* Run python server.py

### Deployment instructions ###

* git pull on production master
* see ./dev/ for config examples (nginx, supervisor)
* edit config.py for other needed options
* setup needed external dependencies (postgreSQL, nginx, supervisor)
